package com.leitnecker.rssreader.app.DataAccessLayer.Mapper;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v4.util.ArrayMap;
import com.leitnecker.rssreader.app.Enums.FeedStatus;
import com.leitnecker.rssreader.app.R;

import java.util.Map;

/**
 * Created by Harald on 21.10.2014.
 */
public class FeedStatusBitmapMapper {
    private static final Map<FeedStatus, Integer> resourceMap;

    static{
        resourceMap=new ArrayMap<FeedStatus, Integer>();
        resourceMap.put(FeedStatus.read, R.drawable.tick_icon);
        resourceMap.put(FeedStatus.unread, R.drawable.new_icon);
        resourceMap.put(FeedStatus.starred, R.drawable.star_icon);
    }
    public static int getImageResourceId(FeedStatus status){
        return resourceMap.get(status);
    }
}
