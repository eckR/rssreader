package com.leitnecker.rssreader.app.Enums;

/**
 * Created by Harald on 16.10.2014.
 */
public enum FRAGMENT {
    SHOW_ARTICLES, SHOW_FEEDS, SUBSCRIBE_FEED
}
