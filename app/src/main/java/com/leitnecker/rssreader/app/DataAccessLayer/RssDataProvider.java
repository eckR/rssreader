package com.leitnecker.rssreader.app.DataAccessLayer;

import android.content.Context;

import com.leitnecker.rssreader.app.DataAccessLayer.Interfaces.IRssDataProvider;
import com.leitnecker.rssreader.app.DataAccessLayer.Interfaces.IRssFeedDataProvider;
import com.leitnecker.rssreader.app.DataAccessLayer.Services.Helper.RssParser;

/**
 * Created by David on 14.10.2014.
 */

public class RssDataProvider implements IRssDataProvider {

    IRssFeedDataProvider rssFeedDataProvider;
    RssParser rssParser;
    Context context;

    public RssDataProvider(Context context, IRssFeedDataProvider rssFeedDataProvider){
        this.rssFeedDataProvider = rssFeedDataProvider;
        this.context = context;

    }

    @Override
    public IRssFeedDataProvider getRssDataProvider() {
        return this.rssFeedDataProvider;
    }
}
