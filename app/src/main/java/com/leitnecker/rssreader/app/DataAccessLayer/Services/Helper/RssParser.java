package com.leitnecker.rssreader.app.DataAccessLayer.Services.Helper;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


import org.jsoup.Jsoup;
import org.xmlpull.v1.XmlPullParser;

import android.util.Xml;

import com.leitnecker.rssreader.app.DataAccessLayer.Interfaces.IRssParser;
import com.leitnecker.rssreader.app.DataAccessLayer.Models.FeedContainer;

import RssData.Feed;
import RssData.FeedItem;

public class RssParser implements IRssParser {

    public List<FeedContainer> parse(String string) throws Exception {
        InputStream stream = new ByteArrayInputStream(string.getBytes());
        return this.parse(stream);
    }

    public List<FeedContainer> parse(InputStream in) throws Exception {
        XmlPullParser parser = Xml.newPullParser();

        //parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(in, "UTF-8");
        parser.nextTag();

        return readChannels(parser);
    }

    private List<FeedContainer> readChannels(XmlPullParser parser) throws Exception {
        ArrayList<FeedContainer> channels = new ArrayList<FeedContainer>();

        parser.require(XmlPullParser.START_TAG, null, "rss");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("channel")) {
                channels.add(readChannel(parser));
                break;

            } else {
                skip(parser);
            }
        }
        return channels;
    }

    private FeedContainer readChannel(XmlPullParser parser) throws Exception {
        Feed c = new Feed();
        List<FeedItem> feedItems = new ArrayList<FeedItem>();

        parser.require(XmlPullParser.START_TAG, null, "channel");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("title")) {
                parser.require(XmlPullParser.START_TAG, null, "title");
                c.setName(readText(parser));
                parser.require(XmlPullParser.END_TAG, null, "title");

            } else if (name.equals("link")) {
                parser.require(XmlPullParser.START_TAG, null, "link");
                c.setUrl(readText(parser));
                parser.require(XmlPullParser.END_TAG, null, "link");

            } else if (name.equals("description")) {
                parser.require(XmlPullParser.START_TAG, null, "description");
                c.setDescription(readText(parser));
                parser.require(XmlPullParser.END_TAG, null, "description");
            } else if (name.equals("item")) {
                FeedItem feetItem = readItem(parser);
                feetItem.setFeed(c);
                feedItems.add(feetItem);

            } else if (name.equals("image")) {
                channelimage image = readchannelimage(parser);
                c.setImageLink(image.link);
                c.setImageUrl(image.url);
                c.setImageTitle(image.title);
                //c.setImageBytes(ImageDownloader.DownloadImage(image.url));
            } else {
                skip(parser);
            }

        }
        return new FeedContainer(c, feedItems);
    }

    private FeedItem readItem(XmlPullParser parser) throws Exception {
        FeedItem i = new FeedItem();
        media m = new media();
        parser.require(XmlPullParser.START_TAG, null, "item");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("title")) {
                i.setTitle(validateString("title", parser));
            } else if (name.equals("link")) {
                i.setLink(validateString("link", parser));
            } else if (name.equals("guid")) {
                i.setGuid(validateString("guid", parser));
            } else if (name.equals("description")) {
                i.setDescription(validateString("description", parser));
            } else if (name.equals("pubDate")) {
                String date = validateString("pubDate", parser);
                DateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH);
                java.util.Date d2 = formatter.parse(date);
                i.setPubDate(d2);
            } else if (name.startsWith("media:")) {
                updatemedia(parser, m, name);
            } else {
                skip(parser);
            }
        }

        //i.item_media = m;
        return i;

    }

    private void skip(XmlPullParser parser) throws Exception {
        int depth = 1;
        while (depth != 0) {
            int c = parser.next();
            if (c == XmlPullParser.START_TAG)
                depth++;
            else if (c == XmlPullParser.END_TAG)
                depth--;
        }
    }

    private String readText(XmlPullParser parser) throws Exception {
        String result = "";

        if (parser.next() == XmlPullParser.TEXT) {


            result = parser.getText();

            int token = parser.nextTag();

            result = Jsoup.parse(result).text();
            if(token==XmlPullParser.CDSECT){
                token = parser.nextToken();
                result = parser.getText();
            }
        }

        return result;
    }

    private String validateString(String s, XmlPullParser parser)
            throws Exception {
        parser.require(XmlPullParser.START_TAG, null, s);
        String result = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, s);
        return result;

    }

    private channelimage readchannelimage(XmlPullParser parser)
            throws Exception {
        channelimage image = new channelimage();
        parser.require(XmlPullParser.START_TAG, null, "image");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("title")) {
                image.title = validateString("title", parser);
            } else if (name.equals("url")) {
                image.url = validateString("url", parser);
            } else if (name.equals("link")) {
                image.link = validateString("link", parser);
            } else {
                skip(parser);
            }
        }
        return image;

    }

    private class channelimage{
        String title;
        String url;
        String link;
    }

    private void updatemedia(XmlPullParser parser, media med, String s)
            throws Exception {
        parser.require(XmlPullParser.START_TAG, null, s);
        String tag = s.substring(s.indexOf(":") + 1, s.length());
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            if (tag.equals("description")) {
                med.description = validateString("media:description", parser);
            } else if (tag.equals("thumbnail")) {
                med.thumbnail_url = parser.getAttributeValue(null, "url");
                med.thumbnail_height = parser.getAttributeValue(null, "height");
                med.thumbnail_width = parser.getAttributeValue(null, "width");
            } else if (tag.equals("content")) {
                med.content_type = parser.getAttributeValue(null, "type");
                med.content_url = parser.getAttributeValue(null, "url");
            } else {
                skip(parser);
            }
        }

    }

    private class media {
        public String description;
        public String thumbnail_url;
        public String thumbnail_width;
        public String thumbnail_height;
        public String content_type;
        public String content_url;
    }

    private static final Map<String, String> DATE_FORMAT_REGEXPS = new HashMap<String, String>() {{
        put("^\\d{8}$", "yyyyMMdd");
        put("^\\d{1,2}-\\d{1,2}-\\d{4}$", "dd-MM-yyyy");
        put("^\\d{4}-\\d{1,2}-\\d{1,2}$", "yyyy-MM-dd");
        put("^\\d{1,2}/\\d{1,2}/\\d{4}$", "MM/dd/yyyy");
        put("^\\d{4}/\\d{1,2}/\\d{1,2}$", "yyyy/MM/dd");
        put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}$", "dd MMM yyyy");
        put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}$", "dd MMMM yyyy");
        put("^\\d{12}$", "yyyyMMddHHmm");
        put("^\\d{8}\\s\\d{4}$", "yyyyMMdd HHmm");
        put("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}$", "dd-MM-yyyy HH:mm");
        put("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}$", "yyyy-MM-dd HH:mm");
        put("^\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}$", "MM/dd/yyyy HH:mm");
        put("^\\d{4}/\\d{1,2}/\\d{1,2}\\s\\d{1,2}:\\d{2}$", "yyyy/MM/dd HH:mm");
        put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}\\s\\d{1,2}:\\d{2}$", "dd MMM yyyy HH:mm");
        put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}$", "dd MMMM yyyy HH:mm");
        put("^\\d{14}$", "yyyyMMddHHmmss");
        put("^\\d{8}\\s\\d{6}$", "yyyyMMdd HHmmss");
        put("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd-MM-yyyy HH:mm:ss");
        put("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}$", "yyyy-MM-dd HH:mm:ss");
        put("^\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "MM/dd/yyyy HH:mm:ss");
        put("^\\d{4}/\\d{1,2}/\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}$", "yyyy/MM/dd HH:mm:ss");
        put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd MMM yyyy HH:mm:ss");
        put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd MMMM yyyy HH:mm:ss");
    }};

    /**
     * Determine SimpleDateFormat pattern matching with the given date string. Returns null if
     * format is unknown. You can simply extend DateUtil with more formats if needed.
     * @param dateString The date string to determine the SimpleDateFormat pattern for.
     * @return The matching SimpleDateFormat pattern, or null if format is unknown.
     * @see SimpleDateFormat
     */
    public static String determineDateFormat(String dateString) {
        for (String regexp : DATE_FORMAT_REGEXPS.keySet()) {
            if (dateString.toLowerCase().matches(regexp)) {
                return DATE_FORMAT_REGEXPS.get(regexp);
            }
        }
        return null; // Unknown format.
    }

}