package com.leitnecker.rssreader.app.DataAccessLayer;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import RssData.DaoMaster;
import RssData.DaoSession;

/**
 * Created by David on 14.10.2014.
 */
public class DatabaseProvider {

    private static DaoSession daoSession;

    public static DaoSession GetDaoSession(Context context){
        if(daoSession == null && context != null){
            DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, "RssData", null);
            SQLiteDatabase db = helper.getWritableDatabase();
            DaoMaster daoMaster = new DaoMaster(db);
            daoSession = daoMaster.newSession();
        }

        return daoSession;
    }

}
