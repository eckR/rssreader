package com.leitnecker.rssreader.app.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.util.SparseBooleanArray;
import android.view.*;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import com.leitnecker.rssreader.app.Contracts.NavigationContract;
import com.leitnecker.rssreader.app.Enums.FRAGMENT;
import com.leitnecker.rssreader.app.ListviewAdapters.FeedCursorAdapter;
import com.leitnecker.rssreader.app.MainActivity;
import com.leitnecker.rssreader.app.R;

public class Fragment_ShowFeeds extends Fragment{
    private ListView listView;
    private FeedCursorAdapter rssFeedCursorAdapter;

    public static Fragment_ShowFeeds newInstance(Bundle extras) {
        Fragment_ShowFeeds fragment = new Fragment_ShowFeeds();
        Bundle args=extras;
        if(args==null){
            args=new Bundle();
        }
        fragment.setArguments(args);
        return fragment;
    }
    public Fragment_ShowFeeds() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View toReturn= inflater.inflate(R.layout.fragment_show_feeds, container, false);
        //Initialise
        listView=(ListView)toReturn.findViewById(R.id.fragment_show_feeds_listview);
        rssFeedCursorAdapter=new FeedCursorAdapter(getActivity().getApplicationContext(),MainActivity.rssDataProvider.getRssDataProvider().getRssFeedsCursor(), R.layout.fragment_show_feeds_rssfeed_item);
        listView.setAdapter(rssFeedCursorAdapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        //Set Listeners
        //MultipleChoiceListener for ActionBar Activities
        listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                int count = listView.getCheckedItemCount();
                mode.setTitle(count + getResources().getString(R.string.cab_checked_string));
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.fragment_show_feeds_cab, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                SparseBooleanArray checkedItems=listView.getCheckedItemPositions();
                for (int i = 0; i < checkedItems.size(); i++) {
                    if(checkedItems.valueAt(i)==true) {
                        rssFeedCursorAdapter.getCursor().moveToPosition(checkedItems.keyAt(i));
                        long _id=rssFeedCursorAdapter.getCursor().getLong(rssFeedCursorAdapter.getCursor().getColumnIndex("_id"));
                        MainActivity.rssDataProvider.getRssDataProvider().unSubscribeRssFeed(_id);
                        //rssFeedArrayAdapter.getItem(checkedItems.keyAt(i)).setName("Zum Löschen "+i);
                    }
                }
                mode.finish();
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        });
        //OnItemClickListener for Fragment Transactions
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = NavigationContract.getNavigationIntent(FRAGMENT.SHOW_ARTICLES);
                Bundle extras = new Bundle();
                rssFeedCursorAdapter.getCursor().moveToPosition(position);
                long _id=rssFeedCursorAdapter.getCursor().getLong(rssFeedCursorAdapter.getCursor().getColumnIndex("_id"));
                extras.putLong("feedId",  _id);
                NavigationContract.sendNavigationIntent(intent, extras, getActivity().getApplicationContext());
            }
        });
        return toReturn;
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }



}