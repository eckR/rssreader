package com.leitnecker.rssreader.app.DataAccessLayer.Interfaces;

/**
 * Created by David on 14.10.2014.
 */
public interface IRssDataProvider {
    public IRssFeedDataProvider getRssDataProvider();

}
