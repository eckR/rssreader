package com.leitnecker.rssreader.app.DataAccessLayer.Interfaces;

import com.leitnecker.rssreader.app.DataAccessLayer.Models.FeedContainer;

import java.util.List;

/**
 * Created by David on 15.10.2014.
 */
public interface IRssParser {
    public List<FeedContainer> parse(String parse) throws Exception;
}
