package com.leitnecker.rssreader.app.LocalBroadcastReceivers;

import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.app.Fragment;
import android.app.FragmentManager;
import com.leitnecker.rssreader.app.Contracts.NavigationContract;
import com.leitnecker.rssreader.app.Enums.FRAGMENT;
import com.leitnecker.rssreader.app.Factories.FragmentFactory;
import com.leitnecker.rssreader.app.R;

/**
 * Created by Harald on 16.10.2014.
 */
public class NavigationReceiver extends BroadcastReceiver {
    private FragmentManager fragmentManager;
    private int container;
    private FragmentManager.BackStackEntry topEntry;
    public NavigationReceiver(FragmentManager fm, int container){
        this.fragmentManager=fm;
        this.container=container;
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        if(NavigationContract.isNavigationIntent(intent)){
            FRAGMENT fragmentEnum = NavigationContract.getFragmentFromIntent(intent);

            Fragment f = FragmentFactory.getFragment(fragmentEnum, intent.getExtras());
            if(fragmentManager.findFragmentByTag(fragmentEnum.toString())!=null){
                f=fragmentManager.findFragmentByTag(fragmentEnum.toString());
            }
            topEntry=null;
            if(f==null || f.isVisible()==false) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(container, f, fragmentEnum.toString());
                if (intent.getExtras().getBoolean(NavigationContract.getIntentBackstackString()) == true) {
                    fragmentTransaction.addToBackStack(fragmentEnum.toString());
                }
                fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

                fragmentTransaction.commit();
            }


        }
    }
}
