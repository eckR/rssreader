package com.leitnecker.rssreader.app.DataAccessLayer.Services;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.leitnecker.rssreader.app.DataAccessLayer.DatabaseProvider;
import com.leitnecker.rssreader.app.DataAccessLayer.Models.FeedContainer;
import com.leitnecker.rssreader.app.DataAccessLayer.Provider.FeedContentProvider.FeedContentProvider;
import com.leitnecker.rssreader.app.DataAccessLayer.Provider.FeedItemContentProvider.FeedItemContentProvider;
import com.leitnecker.rssreader.app.DataAccessLayer.Services.Helper.RssDownloader;
import com.leitnecker.rssreader.app.DataAccessLayer.Services.Helper.RssParser;

import java.io.IOException;
import java.util.List;

import RssData.DaoSession;
import RssData.FeedDao;
import RssData.FeedItem;
import RssData.FeedItemDao;

/**
 * Created by David on 14.10.2014.
 */
public class RssService extends IntentService {
    public static final int SUCCESS = 0;
    public static final int ERROR = 1;

    public RssService() {
        super("RssService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Bundle resultData = new Bundle();
        resultData.putString("download", "");
        resultData.putString("message", "");
        resultData.putLong("feedId", 0);

        ResultReceiver receiver = (ResultReceiver) intent.getParcelableExtra("receiver");
        String url = intent.getStringExtra("url");
        Boolean reload = intent.getBooleanExtra("reload", false);
        Long feedId = intent.getLongExtra("feedId", 0);

        if(reload) resultData.putLong("feedId", feedId);

        DaoSession daoSession = DatabaseProvider.GetDaoSession(null);


        //DOWNLOAD RSS FEED
        RssDownloader rssDownloader = new RssDownloader();

        String downloadedRssContent;
        try {
            downloadedRssContent = rssDownloader.downloadRssString(url);
        } catch (IOException e) {
            resultData.putString("message", "Error at downloading RSS Feed");
            receiver.send(ERROR, resultData);
            return;
        }

        RssParser rssParser = new RssParser();


        //PARSE RSS FEED
        FeedContainer feedContainer;
        try {
            feedContainer = rssParser.parse(downloadedRssContent).get(0);
            feedContainer.getFeed().setUrl(url);
        } catch (Exception e) {
            resultData.putString("message", "Error at parsing RSS Feed");
            receiver.send(ERROR, resultData);
            return;
        }


        //WRITE RSS REED to Database
        //set relation between Feed and FeedItems
        try {

            if(!reload){
                feedContainer.getFeed().setId(daoSession.getFeedDao().insert(feedContainer.getFeed()));
                getContentResolver().notifyChange(FeedContentProvider.CONTENT_URI, null);
                feedId = feedContainer.getFeed().getId();
            }

            for(FeedItem feedItem : feedContainer.getFeedItems()){
                //some RSS Feeds not serve a guid, so check if Guid is empty, and use Link instead
                //of GuId. http://www.w3schools.com/rss/rss_tag_guid.asp
                Boolean guIdIsEmpty = feedItem.getGuid() == null || feedItem.getGuid().isEmpty();
                Boolean linkIsEmpty = feedItem.getLink() == null || feedItem.getLink().isEmpty();

                if(guIdIsEmpty && !linkIsEmpty){
                    feedItem.setGuid(feedItem.getLink());
                }

                feedItem.setFeedId(feedId);

                List<FeedItem> feedItems = daoSession.getFeedItemDao().queryBuilder().where(FeedItemDao.Properties.FeedId.eq(feedId), FeedItemDao.Properties.Guid.eq(feedItem.getGuid())).list();
                Boolean feedItemsContainCurrentFeed = feedItems.size() > 0;


                if(feedItemsContainCurrentFeed && reload){
                    feedItem.setId(feedItems.get(0).getId());
                    feedItem.setStatus(feedItems.get(0).getStatus()); //status should stay the same
                    daoSession.getFeedItemDao().update(feedItem);
                    getContentResolver().notifyChange(FeedItemContentProvider.CONTENT_URI, null);
                }
                else{
                    daoSession.getFeedItemDao().insert(feedItem);
                    getContentResolver().notifyChange(FeedItemContentProvider.CONTENT_URI, null);
                }
            }
        } catch (Exception e) {
            resultData.putString("message", "Error at writing RSS Feed to Db.");
            receiver.send(ERROR, resultData);
            return;
        }


        //Everything was ok,
        resultData.putLong("feedId", feedId);
        resultData.putString("message", "RssFeed was downloaded, parsed and saved successfully");
        receiver.send(SUCCESS, resultData);

    }
}
