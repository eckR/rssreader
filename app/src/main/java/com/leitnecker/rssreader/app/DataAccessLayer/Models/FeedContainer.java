package com.leitnecker.rssreader.app.DataAccessLayer.Models;

import java.util.List;

import RssData.Feed;
import RssData.FeedItem;

/**
 * Created by David on 15.10.2014.
 */
public class FeedContainer {

    private Feed feed;
    private List<FeedItem> feedItems;

    public FeedContainer(Feed feed, List<FeedItem> feeditem){
        this.feed = feed;
        this.feedItems = feeditem;
    }

    public Feed getFeed() {
        return feed;
    }

    public List<FeedItem> getFeedItems() {
        return feedItems;
    }
}
