package com.leitnecker.rssreader.app.DataAccessLayer.Services.Helper;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

/**
 * Created by David on 14.10.2014.
 */
public class RssDownloader {
    public String downloadRssString(String urlToDownload) throws IOException {

        URL url = new URL(urlToDownload);
        URLConnection connection = url.openConnection();
        connection.connect();
        URLConnection connection2 = url.openConnection();
        connection2.connect();

        // download the file
        InputStream inputEncoding = new BufferedInputStream(connection2.getInputStream());
        InputStream input = new BufferedInputStream(connection.getInputStream());

        Scanner s1 = new Scanner(inputEncoding,"utf-8").useDelimiter(">");
        String encodingLine = s1.hasNext() ? s1.next() : "";
        String encoding = "";
        try{
            encoding = encodingLine.split("encoding=\"")[1].split("\"")[0];
        }
        catch(Exception e){
            //std Encoding if no encodign found, use UTF-8
            encoding = "UTF-8";
            Log.v("DownloadService", "Encoding not Found in Downloadstring");
        }


        /*
        funktioniert aus mir unbekannten grund nicht
        Pattern p = Pattern.compile("((encoding=\")([^\"]*))");
        String encoding = p.matcher(encodingLine).group(1);
        */

        //stream to string
        Scanner s = new Scanner(input,encoding).useDelimiter("\\A");
        String downloadString = s.hasNext() ? s.next() : "";

        input.close();

        return downloadString;

    }
}
