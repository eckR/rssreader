package com.leitnecker.rssreader.app.Enums;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by David on 14.10.2014.
 */
public enum FeedStatus {
    unread,
    read,
    starred;

    private static Map<Integer, FeedStatus> map = new HashMap<>();
    static {
        for (FeedStatus status : FeedStatus.values()) {
            map.put(status.ordinal(), status);
        }
    }

    public static FeedStatus forStatus(int i) {
        return map.get(i);
    }
}
