package com.leitnecker.rssreader.app.Contracts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

/**
 * Created by David on 21.10.2014.
 */
public class RssDataTransferContract {

    public static final String RSSFEEDNEWDATA = "rss-feed-new-data";
    public static final String RSSFEEDRELOADDATA = "rss-feed-reload-data";

    //sends to Broadcast-Receiver if SubscribeOperation was ok or not
    public final static void sendNewDataMessage(Context context, Boolean success, String infoText){
        Intent i = new Intent(RSSFEEDNEWDATA);
        i.putExtra("success",success);
        i.putExtra("message", infoText);
        LocalBroadcastManager.getInstance(context).sendBroadcast(i);
    }

    public final static void sendReloadDataMessage(Context context, Boolean success, String infoText){
        sendReloadDataMessage(context,success,infoText,0L);
    }
    public final static void sendReloadDataMessage(Context context, Boolean success, String infoText, Long feedId){
        Intent i = new Intent(RSSFEEDRELOADDATA);
        i.putExtra("success",success);
        i.putExtra("message", infoText);
        i.putExtra("feedId", feedId);
        LocalBroadcastManager.getInstance(context).sendBroadcast(i);
    }

    public static void registerFeedReloadDataBroadcast(BroadcastReceiver broadcastReceiver, Context context){
        LocalBroadcastManager.getInstance(context).registerReceiver(broadcastReceiver, new IntentFilter(RSSFEEDRELOADDATA));
    }
    public static void unregisterFeedReloadDataBroadcast(BroadcastReceiver broadcastReceiver, Context context){
        LocalBroadcastManager.getInstance(context).unregisterReceiver(broadcastReceiver);
    }

    public static void registerFeedNewDataBroadcast(BroadcastReceiver broadcastReceiver, Context context){
        LocalBroadcastManager.getInstance(context).registerReceiver(broadcastReceiver, new IntentFilter(RSSFEEDNEWDATA));
    }
    public static void unregisterFeedNewDataBroadcast(BroadcastReceiver broadcastReceiver, Context context){
        LocalBroadcastManager.getInstance(context).unregisterReceiver(broadcastReceiver);
    }

    public final static boolean checkIntentSuccess(Intent intent){
        return intent.getBooleanExtra("success", false);
    }

    public final static String getIntentMessage(Intent intent){
        return intent.getStringExtra("message");
    }
    public final static Long getIntentFeedId(Intent intent){return intent.getLongExtra("feedId", 0);}
}
