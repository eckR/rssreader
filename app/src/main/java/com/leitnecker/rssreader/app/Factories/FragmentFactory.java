package com.leitnecker.rssreader.app.Factories;

import android.os.Bundle;
import android.app.Fragment;
import com.leitnecker.rssreader.app.Enums.FRAGMENT;
import com.leitnecker.rssreader.app.Fragments.Fragment_ShowArticles;
import com.leitnecker.rssreader.app.Fragments.Fragment_ShowFeeds;
import com.leitnecker.rssreader.app.Fragments.Fragment_SubscribeRss;

/**
 * Created by Harald on 16.10.2014.
 */
public class FragmentFactory {
    public static Fragment getFragment(FRAGMENT fragmentEnum, Bundle extras) {
        Fragment fragment;
        switch(fragmentEnum){
            case SHOW_ARTICLES:
                fragment= Fragment_ShowArticles.newInstance(extras);
                break;
            case SHOW_FEEDS:
                fragment= Fragment_ShowFeeds.newInstance(extras);
                break;
            case SUBSCRIBE_FEED:
                fragment= Fragment_SubscribeRss.newInstance(extras);
                break;
            default:
                fragment=null;
                break;

        }
        return fragment;
    }
}
