package com.leitnecker.rssreader.app.Fragments;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.SparseBooleanArray;
import android.view.*;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import com.leitnecker.rssreader.app.Contracts.RssDataTransferContract;
import com.leitnecker.rssreader.app.DataAccessLayer.Provider.FeedItemContentProvider.FeedItemContentProvider;
import com.leitnecker.rssreader.app.Enums.FeedStatus;
import com.leitnecker.rssreader.app.ListviewAdapters.FeedItemCursorAdapter;
import com.leitnecker.rssreader.app.MainActivity;
import com.leitnecker.rssreader.app.R;

public class Fragment_ShowArticles extends Fragment{


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ShowArticles.
     */
    private ListView lv;
    private FeedItemCursorAdapter feedItemCursorAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private BroadcastReceiver mBroadcastReceiver;
    private ContentObserver mContentObserver;
    private Long feedId;
    public static Fragment_ShowArticles newInstance(Bundle extras) {
        Fragment_ShowArticles fragment = new Fragment_ShowArticles();
        Bundle args=extras;
        if(args==null){
            args=new Bundle();
        }
        fragment.setArguments(args);
        return fragment;
    }
    public Fragment_ShowArticles() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_show_articles, container, false);
        feedId=getArguments().getLong("feedId");


        //Get and hold all the needed Views from the layout
        lv = (ListView) view.findViewById(R.id.fragment_show_articles_listview);
        swipeRefreshLayout=(SwipeRefreshLayout)view.findViewById(R.id.fragment_show_articles_srl);

        //Initialise the observers/receivers/adapters ;)
        //CursorAdapter for ListView
        feedItemCursorAdapter=new FeedItemCursorAdapter(getActivity().getApplicationContext(),
                MainActivity.rssDataProvider.getRssDataProvider().getRssFeedItemsCursor(feedId),R.layout.fragment_show_articles_feed_item);
        //ContentObserver for updating the ListView
        mContentObserver= new ContentObserver(null) {
            @Override
            public boolean deliverSelfNotifications() {

                return super.deliverSelfNotifications();
            }

            @Override
            public void onChange(boolean selfChange) {
                //feedItemCursorAdapter.changeCursor(MainActivity.rssDataProvider.getRssDataProvider().getRssFeedItemsCursor(feedId));
                //feedItemCursorAdapter.notifyDataSetChanged();
            }
        };
        //BroadcastReceiver for disabling refreshing
        mBroadcastReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                swipeRefreshLayout.setRefreshing(false);
                if(RssDataTransferContract.checkIntentSuccess(intent)==false){
                    Toast.makeText(getActivity().getApplicationContext(), getString(R.string.rss_feed_update_fail), Toast.LENGTH_LONG).show();
                }
            }
        };
        //Set all the Listeners
        //OnRefreshListener on SwipeRefreshlayout to kick off updating the RSS-Feed
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(!MainActivity.rssDataProvider.getRssDataProvider().checkIfRssFeedReloads(feedId)) {
                    MainActivity.rssDataProvider.getRssDataProvider().reloadRssFeed(feedId);
                }
            }
        });
        //Onclick ListItems
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                feedItemCursorAdapter.getCursor().moveToPosition(position);
                String guid=feedItemCursorAdapter.getCursor().getString(feedItemCursorAdapter.getCursor().getColumnIndex("LINK"));
                if(guid==null){
                    guid=feedItemCursorAdapter.getCursor().getString(feedItemCursorAdapter.getCursor().getColumnIndex("GUID"));
                }
                long _id=feedItemCursorAdapter.getCursor().getLong(feedItemCursorAdapter.getCursor().getColumnIndex("_id"));

                FeedStatus currentStatus=FeedStatus.forStatus(feedItemCursorAdapter.getCursor().getInt(feedItemCursorAdapter.getCursor().getColumnIndex("STATUS")));
                if(currentStatus!= FeedStatus.starred) {
                    MainActivity.rssDataProvider.getRssDataProvider().changeItemStatus(_id, FeedStatus.read);
                }
                Intent i=new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(guid));
                startActivity(i);
            }
        });
        //MultipleChoiceListener for ActionBar Activities
        lv.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                int count = lv.getCheckedItemCount();
                mode.setTitle(count + getResources().getString(R.string.cab_checked_string));
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.fragment_show_articles_cab, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                FeedStatus status=null;
                switch(item.getItemId()){
                    case R.id.menu_item_article_read:
                        status=FeedStatus.read;
                        break;
                    case R.id.menu_item_article_unread:
                        status=FeedStatus.unread;
                        break;
                    case R.id.menu_item_article_starred:
                        status=FeedStatus.starred;
                        break;
                }
                SparseBooleanArray checkedItems=lv.getCheckedItemPositions();
                for (int i = 0; i < checkedItems.size(); i++) {
                    if(checkedItems.valueAt(i)==true) {
                        feedItemCursorAdapter.getCursor().moveToPosition(checkedItems.keyAt(i));
                        long _id=feedItemCursorAdapter.getCursor().getLong(feedItemCursorAdapter.getCursor().getColumnIndex("_id"));
                        MainActivity.rssDataProvider.getRssDataProvider().changeItemStatus(_id, status);
                    }
                }
                mode.finish();
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        });


        //Set Adapters and Other Stuff on Start
        this.getActivity().getApplicationContext().getContentResolver().registerContentObserver(FeedItemContentProvider.CONTENT_URI, true, mContentObserver);
        RssDataTransferContract.registerFeedReloadDataBroadcast(mBroadcastReceiver, getActivity().getApplicationContext());
        swipeRefreshLayout.setColorSchemeResources(R.color.apptheme_color, R.color.orange, R.color.OrangeDark, R.color.white);
        lv.setAdapter(feedItemCursorAdapter);
        lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        if(MainActivity.rssDataProvider.getRssDataProvider().checkIfRssFeedReloads(feedId)){
            swipeRefreshLayout.setRefreshing(true);
        }

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);


    }

    @Override
    public void onDetach() {
        super.onDetach();
        //unregister Observers/Receivers
        RssDataTransferContract.unregisterFeedReloadDataBroadcast(mBroadcastReceiver, getActivity().getApplicationContext());
        this.getActivity().getContentResolver().unregisterContentObserver(mContentObserver);

    }


}
