package com.leitnecker.rssreader.app;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.leitnecker.rssreader.app.Contracts.NavigationContract;
import com.leitnecker.rssreader.app.DataAccessLayer.DatabaseProvider;
import com.leitnecker.rssreader.app.DataAccessLayer.Provider.FeedContentProvider.FeedContentProvider;
import com.leitnecker.rssreader.app.DataAccessLayer.RssDataProvider;
import com.leitnecker.rssreader.app.DataAccessLayer.RssFeedDataProvider;

import RssData.*;

import RssData.DaoSession;

import com.leitnecker.rssreader.app.LocalBroadcastReceivers.NavigationReceiver;
import com.leitnecker.rssreader.app.Enums.FRAGMENT;


public class MainActivity extends Activity {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationReceiver navigationReceiver;
    public static RssDataProvider rssDataProvider;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaoSession daoSession = DatabaseProvider.GetDaoSession(this);

        this.rssDataProvider=new RssDataProvider(
                getApplicationContext(),
                new RssFeedDataProvider(daoSession, this)
        );




        setContentView(R.layout.activity_main);
        navigationReceiver=new NavigationReceiver(getFragmentManager(),R.id.main_fragment);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            if(intent.getBooleanExtra("success", false)) {
                //Successfull downloaded new RSS FEED
                Log.d("LocalBroadCast:", "added RSS FEED successful :)");
            }
            else {
                //Unsuccessfull downloaded new RSS FEED
                String extrainfo=intent.getStringExtra("infoText");
                Log.d("LocalBroadCast:", "added RSS FEED failed :( \n"+extrainfo);
            }
        }
    };

    @Override
    protected void onDestroy() {
        // Unregister since the activity is about to be closed.
        super.onDestroy();
    }




    @Override
    public void onPause(){
        super.onPause();
        NavigationContract.unregisterBroadcastReceiver(navigationReceiver, getApplicationContext());

    }
    @Override
    public void onResume(){
        super.onResume();
        NavigationContract.registerBroadcastReceiver(navigationReceiver, getApplicationContext());
        if(getFragmentManager().getBackStackEntryCount()==0) {
            Intent intent = NavigationContract.getNavigationIntent(FRAGMENT.SHOW_FEEDS);
            NavigationContract.setAddToBackstack(intent, false);
            NavigationContract.sendNavigationIntent(intent, null, getApplicationContext());
        }



    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
        //return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id==R.id.menu_subscribe_rss){
            Intent intent = NavigationContract.getNavigationIntent(FRAGMENT.SUBSCRIBE_FEED);
            NavigationContract.sendNavigationIntent(intent, null, getApplicationContext());
        }
        else if(id==R.id.menu_show_feeds){
            Intent intent = NavigationContract.getNavigationIntent(FRAGMENT.SHOW_FEEDS);
            NavigationContract.sendNavigationIntent(intent, null, getApplicationContext());
        }
        return super.onOptionsItemSelected(item);
    }
    /**
     * A placeholder fragment containing a simple view.
     */


}
