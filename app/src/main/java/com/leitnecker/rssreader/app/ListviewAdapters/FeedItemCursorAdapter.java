package com.leitnecker.rssreader.app.ListviewAdapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.leitnecker.rssreader.app.DataAccessLayer.Mapper.FeedStatusBitmapMapper;
import com.leitnecker.rssreader.app.Enums.FeedStatus;
import com.leitnecker.rssreader.app.R;

import java.text.SimpleDateFormat;

/**
 * Created by Harald on 21.10.2014.
 */
public class FeedItemCursorAdapter extends CursorAdapter {
    int layout;
    public FeedItemCursorAdapter(Context context, Cursor c, int layout){
        super(context,c, true);
        this.layout=layout;
    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView=inflater.inflate(layout,parent,false);
        ViewHolder holder=new ViewHolder();
        holder.textViewDate=(TextView)convertView.findViewById(R.id.fragment_show_articles_feed_item_date);
        holder.textViewTitle=(TextView)convertView.findViewById(R.id.fragment_show_articles_feed_item_title);
        holder.textViewTime=(TextView)convertView.findViewById(R.id.fragment_show_articles_feed_item_time);
        holder.imageViewStatus=(ImageView)convertView.findViewById(R.id.fragment_show_articles_feed_item_status);
        holder.textViewDescription=(TextView)convertView.findViewById(R.id.fragment_show_articles_feed_item_description);
        convertView.setTag(holder);
        return convertView;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder holder=(ViewHolder)view.getTag();
        FeedStatus status= FeedStatus.forStatus(cursor.getInt(cursor.getColumnIndex("STATUS")));
        int imageResource= FeedStatusBitmapMapper.getImageResourceId(status);
        holder.imageViewStatus.setImageResource(imageResource);
        holder.textViewTitle.setText(cursor.getString(cursor.getColumnIndex("TITLE")));
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd.M.yyyy");
        SimpleDateFormat simpleTimeFormat=new SimpleDateFormat("HH:mm");
        holder.textViewTime.setText(simpleTimeFormat.format(cursor.getLong(cursor.getColumnIndex("PUB_DATE"))));
        holder.textViewDate.setText(simpleDateFormat.format(cursor.getLong(cursor.getColumnIndex("PUB_DATE"))));
        holder.textViewDescription.setText(cursor.getString(cursor.getColumnIndex("DESCRIPTION")));
    }
    private class ViewHolder{
        TextView textViewTitle;
        TextView textViewDate;
        TextView textViewTime;
        TextView textViewDescription;
        ImageView imageViewStatus;
    }
}
