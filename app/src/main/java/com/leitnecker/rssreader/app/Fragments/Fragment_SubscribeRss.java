package com.leitnecker.rssreader.app.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.*;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.leitnecker.rssreader.app.Contracts.RssDataTransferContract;
import com.leitnecker.rssreader.app.MainActivity;
import com.leitnecker.rssreader.app.R;
import org.apache.http.conn.routing.BasicRouteDirector;



public class Fragment_SubscribeRss extends Fragment {

    private EditText urlFeed;
    private ProgressDialog progressDialog;
    private BroadcastReceiver mLocalBroadcastReceiver;
    public static Fragment_SubscribeRss newInstance(Bundle extras) {
        Fragment_SubscribeRss fragment = new Fragment_SubscribeRss();
        Bundle args=extras;
        if(args==null){
            args=new Bundle();
        }
        fragment.setArguments(args);
        return fragment;
    }
    public Fragment_SubscribeRss() {


    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_subscribe_rss, container, false);

        //Get needed Elements from View
        Button subscribeButton=(Button)view.findViewById(R.id.fragment_subscriberss_button_subscribe);
        urlFeed=(EditText)view.findViewById(R.id.fragment_subscriberss_edittext_url);


        //Set Listeners and Receivers
        //OnclickListener for kicking off the adding mechanism
        subscribeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainActivity.rssDataProvider.getRssDataProvider().subscribeRssFeed(urlFeed.getText().toString());
                progressDialog.show();
            }
        });

        //BroadcastReceiver for receiving the messages from RssDataTransferContract
        mLocalBroadcastReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                progressDialog.hide();
                if(intent.getBooleanExtra("success", false)){
                    Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.rss_feed_check_success),Toast.LENGTH_LONG).show();
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.rss_feed_check_fail_message))
                            .setCancelable(false)
                            .setTitle(getResources().getString(R.string.rss_feed_check_fail_title))
                            .setPositiveButton("OK", null);
                    AlertDialog alert = builder.create();
                    alert.show();
                }

            }
        };
        progressDialog=new ProgressDialog(getActivity());
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle(getResources().getString(R.string.check_rss_url_dialog_title));
        progressDialog.setMessage(getResources().getString(R.string.check_rss_url_dialog_message));
        RssDataTransferContract.registerFeedNewDataBroadcast(mLocalBroadcastReceiver, getActivity().getApplicationContext());
        return view;
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        RssDataTransferContract.unregisterFeedNewDataBroadcast(mLocalBroadcastReceiver, getActivity().getApplicationContext());

    }


}
