package com.leitnecker.rssreader.app.Contracts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import com.leitnecker.rssreader.app.Enums.FRAGMENT;

/**
 * Created by Harald on 16.10.2014.
 */
public class NavigationContract {
    public static final String ACTION_NAVIGATION="ACTION_NAVIGATION";
    public static final String INTENT_EXTRA_FRAGMENT="INTENT_EXTRA_FRAGMENT";
    public static final String ADD_TO_BACKSTACK="ADD_TO_BACKSTACK";
    public static boolean isNavigationIntent(Intent intent) {
        if (intent.getAction()==ACTION_NAVIGATION){
            return true;
        }
        return false;
    }

    public static FRAGMENT getFragmentFromIntent(Intent intent) {
        return (FRAGMENT)intent.getExtras().getSerializable(INTENT_EXTRA_FRAGMENT);
    }
    public static final void sendNavigationIntent(Intent intent, Bundle extras, Context c){
        if(intent.getExtras()!=null) intent.putExtras(extras);
        LocalBroadcastManager.getInstance(c).sendBroadcast(intent);
    }
    public static final Intent getNavigationIntent(FRAGMENT fragment){
        Intent intent = new Intent(ACTION_NAVIGATION);
        intent.putExtra(INTENT_EXTRA_FRAGMENT, fragment);
        intent.putExtra(ADD_TO_BACKSTACK, true);
        return intent;
    }
    public static final void registerBroadcastReceiver(BroadcastReceiver br, Context c){
        LocalBroadcastManager.getInstance(c).registerReceiver(br, new IntentFilter(ACTION_NAVIGATION));
    }
    public static final void unregisterBroadcastReceiver(BroadcastReceiver br, Context c){
        LocalBroadcastManager.getInstance(c).unregisterReceiver(br);
    }
    public static final void setAddToBackstack(Intent intent, boolean addToBackstack){
        intent.putExtra(ADD_TO_BACKSTACK, addToBackstack);
    }
    public static final String getIntentBackstackString(){
        return ADD_TO_BACKSTACK;
    }
}
