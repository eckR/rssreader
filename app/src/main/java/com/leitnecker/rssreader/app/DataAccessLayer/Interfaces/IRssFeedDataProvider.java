package com.leitnecker.rssreader.app.DataAccessLayer.Interfaces;

import android.content.ContentProvider;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;

import com.leitnecker.rssreader.app.Enums.FeedStatus;

import java.util.List;

import RssData.Feed;

/**
 * Created by David on 14.10.2014.
 */
public interface IRssFeedDataProvider {
    public void subscribeRssFeed(String url);
    public void unSubscribeRssFeed(long feedId);
    public Cursor getRssFeedsCursor();
    public String getSubscribeMessageLocalBroadcastIntentName();

    public void reloadRssFeed(long feedId);
    public boolean checkIfRssFeedReloads(long feedId);

    public void changeItemStatus(long feedId, FeedStatus status);
    public Cursor getRssFeedItemsCursor(long feedId);
}
