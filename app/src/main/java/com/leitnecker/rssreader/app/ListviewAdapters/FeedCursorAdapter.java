package com.leitnecker.rssreader.app.ListviewAdapters;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;
import com.leitnecker.rssreader.app.R;

/**
 * Created by Harald on 21.10.2014.
 */
public class FeedCursorAdapter extends CursorAdapter{
    int layout;
    public FeedCursorAdapter(Context context, Cursor c, int layout){
        super(context,c, true);
        this.layout=layout;
    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView=inflater.inflate(layout,parent,false);
        ViewHolder holder=new ViewHolder();
        holder.textViewTitle=(TextView) convertView.findViewById(R.id.fragment_navigation_drawer_rssfeed_item_title);
        convertView.setTag(holder);
        return convertView;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        //for(int i = 0; i < cursor.getColumnCount(); i++){
        //    Log.d("COLNAME: ", cursor.getColumnName(i).toString());
        //}
        ViewHolder holder=(ViewHolder)view.getTag();
        holder.textViewTitle.setText(cursor.getString(cursor.getColumnIndex("NAME")));
    }
    private class ViewHolder{
        TextView textViewTitle;
    }
}
