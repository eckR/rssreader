package com.leitnecker.rssreader.app.DataAccessLayer;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

import android.support.v4.content.LocalBroadcastManager;

import com.leitnecker.rssreader.app.Contracts.RssDataTransferContract;
import com.leitnecker.rssreader.app.DataAccessLayer.Interfaces.IRssFeedDataProvider;
import com.leitnecker.rssreader.app.DataAccessLayer.Models.FeedContainer;

import java.util.HashMap;
import java.util.List;

import RssData.DaoSession;
import RssData.Feed;
import RssData.FeedItem;

import com.leitnecker.rssreader.app.DataAccessLayer.Provider.FeedContentProvider.FeedContentProvider;
import com.leitnecker.rssreader.app.DataAccessLayer.Provider.FeedItemContentProvider.FeedItemContentProvider;
import com.leitnecker.rssreader.app.DataAccessLayer.Services.Helper.RssDownloader;
import com.leitnecker.rssreader.app.DataAccessLayer.Services.Helper.RssParser;
import com.leitnecker.rssreader.app.DataAccessLayer.Services.RssService;
import com.leitnecker.rssreader.app.Enums.FeedStatus;

/**
 * Created by David on 14.10.2014.
 */
public class RssFeedDataProvider implements IRssFeedDataProvider {

    private DaoSession daoSession;
    private Context context;
    private ContentResolver contentResolver;
    private Uri feedsContentProviderUri;
    private Uri feedItemsContentProviderUri;
    private HashMap<Long, Boolean> loadingRssFeeds;

    public RssFeedDataProvider(DaoSession daoSession, Context context){
        this.context = context;
        this.daoSession = daoSession;
        this.loadingRssFeeds = new HashMap<Long, Boolean>();

        //this.subscribeRssFeed("http://serienjunkies.org/xml/feeds/episoden.xml");
        //this.subscribeRssFeed("http://static.winfuture.de/feeds/WinFuture-News-rss2.0.xml");
        //this.subscribeRssFeed("http://heise.de.feedsportal.com/c/35207/f/653901/index.rss");

        FeedContentProvider.daoSession = daoSession;
        FeedItemContentProvider.daoSession = daoSession;
        this.contentResolver = context.getContentResolver();
        this.feedsContentProviderUri = FeedContentProvider.CONTENT_URI;
        this.feedItemsContentProviderUri = FeedItemContentProvider.CONTENT_URI;
    }

    @Override
    /*
    subscribe to an RssFeed and Download all the FeedItems
     */
    public void subscribeRssFeed(String url) {
        Intent intent = new Intent(context, RssService.class);
        intent.putExtra("url", url);
        intent.putExtra("reload", false);
        intent.putExtra("receiver", new DownloadReceiver(new Handler()));
        context.startService(intent);
    }

    @Override
    /*

     */
    public void reloadRssFeed(long feedId) {
        loadingRssFeeds.put(feedId, true);

        Cursor c = contentResolver.query(this.feedsContentProviderUri, null, "_id=?",new String[]{Long.toString(feedId)}, null);

        c.moveToFirst();
        String url = c.getString(c.getColumnIndex("URL"));

        Intent intent = new Intent(context, RssService.class);
        intent.putExtra("url", url);
        intent.putExtra("reload", true);
        intent.putExtra("feedId", feedId);
        intent.putExtra("receiver", new RefreshReceiver(new Handler()));
        context.startService(intent);
    }


    @Override
    /*
    get the Intent for Local-Broadcast-Receiver to receive Changes on adding RssFeed
     */
    public String getSubscribeMessageLocalBroadcastIntentName(){
        return "Subscribe-Message-Rss-Feed";
    }

    @Override
    public boolean checkIfRssFeedReloads(long feedId) {

        if (!loadingRssFeeds.containsKey(feedId)){
            return false;
        }

        return loadingRssFeeds.get(feedId);
    }

    @Override
    public void changeItemStatus(long feedItemId, FeedStatus status) {

        ContentValues contentValues = new ContentValues();
        contentValues.put("STATUS", status.ordinal());

        this.contentResolver.update(this.feedItemsContentProviderUri, contentValues,"_id=?", new String[]{Long.toString(feedItemId)});

    }

    @Override
    public Cursor getRssFeedItemsCursor(long feedId) {
        return contentResolver.query(this.feedItemsContentProviderUri, null, "FEED_ID=?",new String[]{Long.toString(feedId)}, "PUB_DATE DESC");
    }

    @Override
    /*
    un-subscribe from a RSSFeed and delete it, and all it's items from Database
     */
    public void unSubscribeRssFeed(long feedId) {
        this.contentResolver.delete(this.feedsContentProviderUri,"_id=?",new String[]{Long.toString(feedId)});
        this.contentResolver.delete(this.feedItemsContentProviderUri, "FEED_ID=?", new String[]{Long.toString(feedId)});
    }

    @Override
    /*
    Returns a Cursor of all RssFeeds which are in Database at the Moment.
     */
    public Cursor getRssFeedsCursor() {
        return contentResolver.query(this.feedsContentProviderUri,null, null, null, null);
    }


    //gets called if Service is finished (at subscribing)
    private class DownloadReceiver extends ResultReceiver {
        public DownloadReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);

            String message = resultData.getString("message");

            if (resultCode == RssService.SUCCESS) {
                RssDataTransferContract.sendNewDataMessage(context, true, "Successfully added RSS Feed");
            }
            else{
                RssDataTransferContract.sendNewDataMessage(context, false, "Error while adding RSS Feed. ("+message+")");
            }
        }
    }

    //get called if Service is finished (at reloading)
    private class RefreshReceiver extends ResultReceiver {
        public RefreshReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);

            String message = resultData.getString("message");

            long feedId = resultData.getLong("feedId",0);
            loadingRssFeeds.put(feedId, false);

            if (resultCode == RssService.SUCCESS) {
                RssDataTransferContract.sendReloadDataMessage(context, true, "Successfully refreshed RSS Feed", feedId);
            }
            else{
                RssDataTransferContract.sendReloadDataMessage(context, false, "Error while refreshing RSS Feed. ("+message+")");
            }
        }
    }

}
