package RssData;

import java.util.List;
import java.util.ArrayList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.SqlUtils;
import de.greenrobot.dao.internal.DaoConfig;

import RssData.FeedItem;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table FEED_ITEM.
*/
public class FeedItemDao extends AbstractDao<FeedItem, Long> {

    public static final String TABLENAME = "FEED_ITEM";

    /**
     * Properties of entity FeedItem.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property FeedId = new Property(1, Long.class, "feedId", false, "FEED_ID");
        public final static Property Title = new Property(2, String.class, "title", false, "TITLE");
        public final static Property Status = new Property(3, Integer.class, "status", false, "STATUS");
        public final static Property Link = new Property(4, String.class, "link", false, "LINK");
        public final static Property Description = new Property(5, String.class, "description", false, "DESCRIPTION");
        public final static Property Author = new Property(6, String.class, "author", false, "AUTHOR");
        public final static Property Category = new Property(7, String.class, "category", false, "CATEGORY");
        public final static Property Comments = new Property(8, String.class, "comments", false, "COMMENTS");
        public final static Property Enclosure = new Property(9, String.class, "enclosure", false, "ENCLOSURE");
        public final static Property Guid = new Property(10, String.class, "guid", false, "GUID");
        public final static Property PubDate = new Property(11, java.util.Date.class, "pubDate", false, "PUB_DATE");
        public final static Property Source = new Property(12, String.class, "source", false, "SOURCE");
        public final static Property Thumbnail_url = new Property(13, String.class, "thumbnail_url", false, "THUMBNAIL_URL");
        public final static Property Thumbnail_width = new Property(14, String.class, "thumbnail_width", false, "THUMBNAIL_WIDTH");
        public final static Property Thumbnail_height = new Property(15, String.class, "thumbnail_height", false, "THUMBNAIL_HEIGHT");
        public final static Property Content_type = new Property(16, String.class, "content_type", false, "CONTENT_TYPE");
        public final static Property Content_url = new Property(17, String.class, "content_url", false, "CONTENT_URL");
    };

    private DaoSession daoSession;


    public FeedItemDao(DaoConfig config) {
        super(config);
    }
    
    public FeedItemDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'FEED_ITEM' (" + //
                "'_id' INTEGER PRIMARY KEY ," + // 0: id
                "'FEED_ID' INTEGER," + // 1: feedId
                "'TITLE' TEXT," + // 2: title
                "'STATUS' INTEGER," + // 3: status
                "'LINK' TEXT," + // 4: link
                "'DESCRIPTION' TEXT," + // 5: description
                "'AUTHOR' TEXT," + // 6: author
                "'CATEGORY' TEXT," + // 7: category
                "'COMMENTS' TEXT," + // 8: comments
                "'ENCLOSURE' TEXT," + // 9: enclosure
                "'GUID' TEXT," + // 10: guid
                "'PUB_DATE' INTEGER," + // 11: pubDate
                "'SOURCE' TEXT," + // 12: source
                "'THUMBNAIL_URL' TEXT," + // 13: thumbnail_url
                "'THUMBNAIL_WIDTH' TEXT," + // 14: thumbnail_width
                "'THUMBNAIL_HEIGHT' TEXT," + // 15: thumbnail_height
                "'CONTENT_TYPE' TEXT," + // 16: content_type
                "'CONTENT_URL' TEXT);"); // 17: content_url
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'FEED_ITEM'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, FeedItem entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        Long feedId = entity.getFeedId();
        if (feedId != null) {
            stmt.bindLong(2, feedId);
        }
 
        String title = entity.getTitle();
        if (title != null) {
            stmt.bindString(3, title);
        }
 
        Integer status = entity.getStatus();
        if (status != null) {
            stmt.bindLong(4, status);
        }
 
        String link = entity.getLink();
        if (link != null) {
            stmt.bindString(5, link);
        }
 
        String description = entity.getDescription();
        if (description != null) {
            stmt.bindString(6, description);
        }
 
        String author = entity.getAuthor();
        if (author != null) {
            stmt.bindString(7, author);
        }
 
        String category = entity.getCategory();
        if (category != null) {
            stmt.bindString(8, category);
        }
 
        String comments = entity.getComments();
        if (comments != null) {
            stmt.bindString(9, comments);
        }
 
        String enclosure = entity.getEnclosure();
        if (enclosure != null) {
            stmt.bindString(10, enclosure);
        }
 
        String guid = entity.getGuid();
        if (guid != null) {
            stmt.bindString(11, guid);
        }
 
        java.util.Date pubDate = entity.getPubDate();
        if (pubDate != null) {
            stmt.bindLong(12, pubDate.getTime());
        }
 
        String source = entity.getSource();
        if (source != null) {
            stmt.bindString(13, source);
        }
 
        String thumbnail_url = entity.getThumbnail_url();
        if (thumbnail_url != null) {
            stmt.bindString(14, thumbnail_url);
        }
 
        String thumbnail_width = entity.getThumbnail_width();
        if (thumbnail_width != null) {
            stmt.bindString(15, thumbnail_width);
        }
 
        String thumbnail_height = entity.getThumbnail_height();
        if (thumbnail_height != null) {
            stmt.bindString(16, thumbnail_height);
        }
 
        String content_type = entity.getContent_type();
        if (content_type != null) {
            stmt.bindString(17, content_type);
        }
 
        String content_url = entity.getContent_url();
        if (content_url != null) {
            stmt.bindString(18, content_url);
        }
    }

    @Override
    protected void attachEntity(FeedItem entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public FeedItem readEntity(Cursor cursor, int offset) {
        FeedItem entity = new FeedItem( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getLong(offset + 1), // feedId
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // title
            cursor.isNull(offset + 3) ? null : cursor.getInt(offset + 3), // status
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // link
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // description
            cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6), // author
            cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7), // category
            cursor.isNull(offset + 8) ? null : cursor.getString(offset + 8), // comments
            cursor.isNull(offset + 9) ? null : cursor.getString(offset + 9), // enclosure
            cursor.isNull(offset + 10) ? null : cursor.getString(offset + 10), // guid
            cursor.isNull(offset + 11) ? null : new java.util.Date(cursor.getLong(offset + 11)), // pubDate
            cursor.isNull(offset + 12) ? null : cursor.getString(offset + 12), // source
            cursor.isNull(offset + 13) ? null : cursor.getString(offset + 13), // thumbnail_url
            cursor.isNull(offset + 14) ? null : cursor.getString(offset + 14), // thumbnail_width
            cursor.isNull(offset + 15) ? null : cursor.getString(offset + 15), // thumbnail_height
            cursor.isNull(offset + 16) ? null : cursor.getString(offset + 16), // content_type
            cursor.isNull(offset + 17) ? null : cursor.getString(offset + 17) // content_url
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, FeedItem entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setFeedId(cursor.isNull(offset + 1) ? null : cursor.getLong(offset + 1));
        entity.setTitle(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setStatus(cursor.isNull(offset + 3) ? null : cursor.getInt(offset + 3));
        entity.setLink(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setDescription(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setAuthor(cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6));
        entity.setCategory(cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7));
        entity.setComments(cursor.isNull(offset + 8) ? null : cursor.getString(offset + 8));
        entity.setEnclosure(cursor.isNull(offset + 9) ? null : cursor.getString(offset + 9));
        entity.setGuid(cursor.isNull(offset + 10) ? null : cursor.getString(offset + 10));
        entity.setPubDate(cursor.isNull(offset + 11) ? null : new java.util.Date(cursor.getLong(offset + 11)));
        entity.setSource(cursor.isNull(offset + 12) ? null : cursor.getString(offset + 12));
        entity.setThumbnail_url(cursor.isNull(offset + 13) ? null : cursor.getString(offset + 13));
        entity.setThumbnail_width(cursor.isNull(offset + 14) ? null : cursor.getString(offset + 14));
        entity.setThumbnail_height(cursor.isNull(offset + 15) ? null : cursor.getString(offset + 15));
        entity.setContent_type(cursor.isNull(offset + 16) ? null : cursor.getString(offset + 16));
        entity.setContent_url(cursor.isNull(offset + 17) ? null : cursor.getString(offset + 17));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(FeedItem entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(FeedItem entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
    private String selectDeep;

    protected String getSelectDeep() {
        if (selectDeep == null) {
            StringBuilder builder = new StringBuilder("SELECT ");
            SqlUtils.appendColumns(builder, "T", getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T0", daoSession.getFeedDao().getAllColumns());
            builder.append(" FROM FEED_ITEM T");
            builder.append(" LEFT JOIN FEED T0 ON T.'FEED_ID'=T0.'_id'");
            builder.append(' ');
            selectDeep = builder.toString();
        }
        return selectDeep;
    }
    
    protected FeedItem loadCurrentDeep(Cursor cursor, boolean lock) {
        FeedItem entity = loadCurrent(cursor, 0, lock);
        int offset = getAllColumns().length;

        Feed feed = loadCurrentOther(daoSession.getFeedDao(), cursor, offset);
        entity.setFeed(feed);

        return entity;    
    }

    public FeedItem loadDeep(Long key) {
        assertSinglePk();
        if (key == null) {
            return null;
        }

        StringBuilder builder = new StringBuilder(getSelectDeep());
        builder.append("WHERE ");
        SqlUtils.appendColumnsEqValue(builder, "T", getPkColumns());
        String sql = builder.toString();
        
        String[] keyArray = new String[] { key.toString() };
        Cursor cursor = db.rawQuery(sql, keyArray);
        
        try {
            boolean available = cursor.moveToFirst();
            if (!available) {
                return null;
            } else if (!cursor.isLast()) {
                throw new IllegalStateException("Expected unique result, but count was " + cursor.getCount());
            }
            return loadCurrentDeep(cursor, true);
        } finally {
            cursor.close();
        }
    }
    
    /** Reads all available rows from the given cursor and returns a list of new ImageTO objects. */
    public List<FeedItem> loadAllDeepFromCursor(Cursor cursor) {
        int count = cursor.getCount();
        List<FeedItem> list = new ArrayList<FeedItem>(count);
        
        if (cursor.moveToFirst()) {
            if (identityScope != null) {
                identityScope.lock();
                identityScope.reserveRoom(count);
            }
            try {
                do {
                    list.add(loadCurrentDeep(cursor, false));
                } while (cursor.moveToNext());
            } finally {
                if (identityScope != null) {
                    identityScope.unlock();
                }
            }
        }
        return list;
    }
    
    protected List<FeedItem> loadDeepAllAndCloseCursor(Cursor cursor) {
        try {
            return loadAllDeepFromCursor(cursor);
        } finally {
            cursor.close();
        }
    }
    

    /** A raw-style query where you can pass any WHERE clause and arguments. */
    public List<FeedItem> queryDeep(String where, String... selectionArg) {
        Cursor cursor = db.rawQuery(getSelectDeep() + where, selectionArg);
        return loadDeepAllAndCloseCursor(cursor);
    }
 
}
