package com.leitnecker.rssreader.greenDao;

import de.greenrobot.daogenerator.Schema;
import de.greenrobot.daogenerator.*;

public class MyDaoGenerator {

    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(3, "RssData");

        //RssFeeds
        Entity feed = schema.addEntity("Feed");
        feed.addIdProperty();
        feed.addStringProperty("name");
        feed.addStringProperty("url");
        feed.addStringProperty("description");
        feed.addStringProperty("imageTitle");
        feed.addStringProperty("imageUrl");
        feed.addStringProperty("imageLink");
        feed.addByteArrayProperty("imageBytes");
        feed.addStringProperty("author");
        feed.addContentProvider();

        //RssFeedItems
        Entity feedItem = schema.addEntity("FeedItem");
        feedItem.addIdProperty();
        Property feedIdProperty = feedItem.addLongProperty("feedId").getProperty();
        feedItem.addToOne(feed, feedIdProperty);
        feedItem.addStringProperty("title");
        feedItem.addIntProperty("status");
        feedItem.addStringProperty("link");
        feedItem.addStringProperty("description");
        feedItem.addStringProperty("author");
        feedItem.addStringProperty("category");
        feedItem.addStringProperty("comments");
        feedItem.addStringProperty("enclosure");
        feedItem.addStringProperty("guid");
        feedItem.addDateProperty("pubDate");
        feedItem.addStringProperty("source");
        feedItem.addStringProperty("thumbnail_url");
        feedItem.addStringProperty("thumbnail_width");
        feedItem.addStringProperty("thumbnail_height");
        feedItem.addStringProperty("content_type");
        feedItem.addStringProperty("content_url");
        feedItem.addContentProvider();

        new DaoGenerator().generateAll(schema, args[0]);
    }
}
